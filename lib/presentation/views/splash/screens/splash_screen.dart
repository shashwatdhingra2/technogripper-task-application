import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:task_app/constants/constants.dart';
import 'package:task_app/utils/routes/routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    navigateToHome();
  }

  void navigateToHome() {
    Timer(
      const Duration(seconds: 3),
      () {
        Navigator.pushNamedAndRemoveUntil(
            context, RoutesName.homeScreen, (route) => false);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Shimmer.fromColors(
            baseColor: Colors.blue.shade700,
            highlightColor: Colors.white,
            loop: 3,
            child: Image.asset(technoGripperLogo, width: size.width * 0.5)),
      ),
    );
  }
}
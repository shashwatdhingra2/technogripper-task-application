import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:task_app/constants/constants.dart';
import 'package:task_app/presentation/view_models/home_view_model.dart';
import 'package:task_app/utils/utils.dart';

AppBar getHomeAppBar() {
  return AppBar(
      elevation: 2,
      shadowColor: Colors.grey.shade100,
      surfaceTintColor: Colors.white,
      leading: Image.asset(technoGripperLogo),
      title: const Text('Techno Gripper'),
      actions: [
        Consumer<HomeViewModel>(
            builder: (context, value, child) => IconButton(
                onPressed: () async {
                  showImportDialog(context, value);
                },
                icon: Icon(Icons.download, color: Colors.grey.shade500))),
        Consumer<HomeViewModel>(
            builder: (context, value, child) => value.isInternetConnected
                ? const Icon(
                    Icons.wifi,
                    color: Colors.green,
                  )
                : const Icon(Icons.wifi_off_sharp, color: Colors.red)),
        12.pw
      ]);
}

void showImportDialog(BuildContext context, HomeViewModel homeViewModel) async {
  showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: const Text('Want to import Cloud Notes ?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('No')),
              TextButton(
                  onPressed: () async {
                    await homeViewModel.importNotesFromCloud();
                    Navigator.pop(context);
                  },
                  child: const Text('Yes')),
            ],
          ));
}

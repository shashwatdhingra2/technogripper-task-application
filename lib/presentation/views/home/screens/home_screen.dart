import 'dart:async';
import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:provider/provider.dart';
import 'package:task_app/presentation/view_models/home_view_model.dart';
import 'package:task_app/presentation/views/home/widgets/app_bar.dart';
import 'package:intl/intl.dart';
import 'package:task_app/utils/routes/routes.dart';
import 'package:task_app/utils/utils.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late StreamSubscription<List<ConnectivityResult>> subscription;

  @override
  void initState() {
    super.initState();

    // Setting up a Stream to listen network connectivity.
    final homeViewModel = Provider.of<HomeViewModel>(context, listen: false);
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((List<ConnectivityResult> result) {
      homeViewModel.updateInternetConnection(result);
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getHomeAppBar(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Greeting to User

              ListTile(
                title: const Text(
                  'Hello, User',
                  style: TextStyle(fontSize: 34),
                ),
                subtitle: Text(
                  DateFormat('MMMM d').format(DateTime.now()),
                  style: const TextStyle(fontSize: 26),
                ),
              ),
              24.ph,
              Expanded(
                child: Consumer<HomeViewModel>(
                  builder: (context, value, child) => value.tasks != null
                      ? Column(
                          children: [
                            if (!value.isInternetConnected)
                              const Text(
                                'No Inter-Connection!\nBut you can still make notes!',
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 18),
                              ),
                            Expanded(
                              child: ListView.builder(
                                  itemBuilder: (context, index) {
                                    final task = value.tasks![index];
                                    return ListTile(
                                      leading: IconButton(
                                          icon: Icon(task.isCompleted
                                              ? Icons.check_box_outlined
                                              : Icons.check_box_outline_blank),
                                          onPressed: () {
                                            value.toggleTaskComplete(task);
                                          }),
                                      title: Text(task.title),
                                      trailing: IconButton(
                                          icon: Icon(Icons.delete_outlined),
                                          onPressed: () {
                                            showDialog(
                                                context: context,
                                                builder: (context) =>
                                                    AlertDialog(
                                                      title:
                                                          Text('Delete Task ?'),
                                                      actions: [
                                                        TextButton(
                                                            onPressed:
                                                                () async {
                                                              await value
                                                                  .deleteTask(
                                                                      task,
                                                                      deleteLocallyOnly:
                                                                          true);
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            child: Text(
                                                                'Delete Locally')),
                                                        TextButton(
                                                            onPressed:
                                                                () async {
                                                              await value.deleteTask(
                                                                  task,
                                                                  deleteLocallyOnly:
                                                                      false);
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            child: Text(
                                                                'Delete everywhere'))
                                                      ],
                                                    ));
                                          }),
                                    );
                                  },
                                  itemCount: value.tasks!.length),
                            ),
                          ],
                        )
                      : const Center(child: Text('Manage your Tasks here!')),
                ),
              )
            ]),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            openDialogBox(context);
          },
          shape: const CircleBorder(),
          child: const Icon(Icons.add)),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

void openDialogBox(BuildContext context) {
  final homeViewModel = Provider.of<HomeViewModel>(context, listen: false);
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        content: SingleChildScrollView(
          child: Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: const Icon(Icons.cancel),
                      ),
                      const SizedBox(
                        width: 54,
                      ),
                      const Text('Add Todo Task '),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 2.0)),
                    child: TextField(
                      controller: homeViewModel.taskController,
                      decoration: const InputDecoration(
                          border: InputBorder.none, hintText: 'Enter the task'),
                    ),
                  ),
                  24.ph,
                  CupertinoButton(
                    onPressed: () async {
                      await homeViewModel.addTaskButtonListner();
                      Navigator.pop(context);
                    },
                    color: Colors.deepPurple.shade400,
                    child: homeViewModel.isLoading
                        ? const CircularProgressIndicator()
                        : const Text('Add'),
                  )
                ]),
          ),
        ),
      );
    },
  );
}

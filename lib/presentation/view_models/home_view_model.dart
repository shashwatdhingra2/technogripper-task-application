import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:task_app/models/task_model.dart';
import 'package:task_app/repository/firebase_repository.dart';
import 'package:task_app/utils/helper.dart';
import 'package:random_string/random_string.dart';
import 'package:task_app/utils/utils.dart';

class HomeViewModel extends ChangeNotifier {
  // Variables
  bool isLoading = false;
  bool isInternetConnected = false;
  List<TaskModel>? tasks;
  TextEditingController taskController = TextEditingController();
  QuerySnapshot? querySnapshot;

  // Classes
  FirebaseRepository firebaseRepository = FirebaseRepository();
  DBHelper dbHelper = DBHelper();

  HomeViewModel() {
    checkInternetConnection();
  }

  Future<void> updateInternetConnection(List<ConnectivityResult> result) async {
    log('Updating Internet Connection Flag');
    if (result.contains(ConnectivityResult.none)) {
      isInternetConnected = false;
      notifyListeners();
    } else {
      isInternetConnected = true;
      notifyListeners();
      await getTasks();
      await syncData();
    }
  }

  Future<void> checkInternetConnection() async {
    final List<ConnectivityResult> connectivityResult =
        await (Connectivity().checkConnectivity());

    if (connectivityResult.contains(ConnectivityResult.none)) {
      isInternetConnected = false;
    } else {
      isInternetConnected = true;
    }
    await getTasks();
  }

  Future<void> syncData() async {
    QuerySnapshot tempQuerySnapshot = await firebaseRepository.getTasks();
    if (tasks == null || tasks!.isEmpty) {
      return;
    }
    for (TaskModel task in tasks!) {
      for (QueryDocumentSnapshot doc in tempQuerySnapshot.docs) {
        if (task.id != (doc.data() as Map)['id']) {
          await firebaseRepository.createTask(task);
        }
      }
    }
  }

  Future<void> getTasks() async {
    toggleLoading(true);
    try {
      tasks = await dbHelper.getTasks();
      notifyListeners();
    } catch (e) {
      print(e.toString());
    } finally {
      toggleLoading(false);
    }
  }

  // Future<void>

  Future<void> addTaskButtonListner() async {
    toggleLoading(true);
    try {
      // Create task model
      final newTask = TaskModel(
          id: randomAlphaNumeric(10),
          title: taskController.text.toString(),
          time: DateTime.now());
      if (isInternetConnected) {
        await firebaseRepository.createTask(newTask);
        await dbHelper.addTask(newTask);
        await getTasks();
      } else {
        await dbHelper.addTask(newTask);
        await getTasks();
      }
      taskController.clear();
    } catch (e) {
      log(e.toString());
    } finally {
      toggleLoading(false);
    }
  }

  Future<void> deleteTask(TaskModel task,
      {bool deleteLocallyOnly = true}) async {
    if (!deleteLocallyOnly && !isInternetConnected) {
      Utils.showToast('No Internet Connection', true);
      return;
    }
    if (deleteLocallyOnly) {
      dbHelper.deleteTask(task.id);
    } else {
      await firebaseRepository.deleteTask(task.id);
      await dbHelper.deleteTask(task.id);
    }
    getTasks();
  }

  Future<void> toggleTaskComplete(TaskModel task) async {
    task.isCompleted = !task.isCompleted;
    if (isInternetConnected) {
      dbHelper.updateTask(task);
      firebaseRepository.updateTask(task);
    } else {
      dbHelper.updateTask(task);
    }
    getTasks();
  }

  Future<void> importNotesFromCloud() async {
    querySnapshot = await firebaseRepository.getTasks();
    if (querySnapshot!.docs.isEmpty) {
      Utils.showToast('No Task Available on Cloud.', false);
    }
    if (tasks == null || tasks!.isEmpty) {
      for (QueryDocumentSnapshot doc in querySnapshot!.docs) {
        await dbHelper
            .addTask(TaskModel.fromMap(doc.data() as Map<String, dynamic>));
      }
      getTasks();
      return;
    }
    for (QueryDocumentSnapshot doc in querySnapshot!.docs) {
      for (TaskModel task in tasks!) {
        if (task.id != (doc.data() as Map)['id']) {
          await dbHelper
              .addTask(TaskModel.fromMap(doc.data() as Map<String, dynamic>));
        }
      }
    }

    getTasks();
  }

  void toggleLoading(bool value) {
    isLoading = value;
    notifyListeners();
  }
}

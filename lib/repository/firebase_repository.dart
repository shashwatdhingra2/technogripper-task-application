import 'package:task_app/models/task_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseRepository {
  FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  String notesCollection = 'Notes';

  Future<void> createTask(TaskModel task) async {
    return firebaseFirestore
        .collection(notesCollection)
        .doc(task.id)
        .set(task.toMap());
  }

  Future<QuerySnapshot> getTasks()async{
    return await firebaseFirestore.collection(notesCollection).get();
  }

  Future<void> deleteTask(String id)async{
    firebaseFirestore.collection(notesCollection).doc(id).delete();
  }

  Future<void> updateTask(TaskModel task)async{
    firebaseFirestore.collection(notesCollection).doc(task.id).update(task.toMap());
  }
}

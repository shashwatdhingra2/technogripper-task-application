import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:task_app/presentation/view_models/home_view_model.dart';
import 'package:task_app/utils/routes/route_generator.dart';
import 'package:task_app/utils/routes/routes_name.dart';
import 'firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async  {

  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => HomeViewModel())],
      child: MaterialApp(
        title: 'TechnoGripper Task App',
        themeMode: ThemeMode.dark,
        theme: ThemeData(
          brightness: Brightness.dark,
          useMaterial3: true,
        ),
        onGenerateRoute: RouteGenerator.generateRoute,
        initialRoute: RoutesName.splashScreen,
      ),
    );
  }
}

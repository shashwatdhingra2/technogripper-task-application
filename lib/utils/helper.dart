import 'dart:ffi';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:task_app/models/task_model.dart';

class DBHelper {
  static const String APP_DB = 'app_database';
  static const String CHECKIN_TABLE = 'task_table';

  static Database? _db;

  // Private Contructor
  DBHelper._privateConstructor();

  // Singleton instance
  static final DBHelper _instatce = DBHelper._privateConstructor();

  // Getter to access the singleton instance
  factory DBHelper() {
    return _instatce;
  }

  Future<Database> get getDatabase async {
    if (_db != null) return _db!;
    _db = await initDatabase();
    return _db!;
  }

  Future<Database> initDatabase() async {
    String path = join(await getDatabasesPath(), '$APP_DB.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE $CHECKIN_TABLE(id TEXT PRIMARY KEY, title TEXT, isCompleted INTEGER DEFAULT 0, time TEXT)');
  }

  Future<void> addTask(TaskModel data) async {
    final db = await getDatabase;
    await db.insert(
      CHECKIN_TABLE,
      data.toMap(),
    );
  }

  Future<List<TaskModel>> getTasks() async {
    final db = await getDatabase;
    final List<Map<String, dynamic>> data = await db.query(CHECKIN_TABLE);
    return data.map((item) => TaskModel.fromMap(item)).toList();
  }

  Future<void> deleteTask(String id) async {
    final db = await getDatabase;
    await db.delete(
      CHECKIN_TABLE,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<void> updateTask(TaskModel task) async {
    final db = await getDatabase;
    await db.update(CHECKIN_TABLE, task.toMap(),
        where: 'id = ?', whereArgs: [task.id]);
  }

  Future<void> clearDatabase() async {
    final db = await getDatabase;
    db.delete(CHECKIN_TABLE);
  }
}

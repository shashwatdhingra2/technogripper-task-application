// Barrel File

export 'package:task_app/presentation/views/home/screens/home_screen.dart';
export 'package:task_app/presentation/views/splash/screens/splash_screen.dart';
export 'routes_name.dart';
export 'package:flutter/cupertino.dart';
export 'package:flutter/material.dart' show Scaffold, AppBar;
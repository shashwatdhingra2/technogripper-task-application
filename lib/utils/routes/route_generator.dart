import 'routes.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RoutesName.splashScreen:
        return getCustomRoute(const SplashScreen());

      case RoutesName.homeScreen:
        return getCustomRoute(const HomeScreen());

      default:
        return _errorRoute();
    }
  }

  static CupertinoPageRoute getCustomRoute(Widget screen) {
    return CupertinoPageRoute(builder: (context) => screen);
  }

  static Route<dynamic> _errorRoute() {
    return CupertinoPageRoute(
      builder: (context) {
        return Scaffold(
            appBar: AppBar(title: const Text('ERROR')),
            body: const Center(child: Text('ERROR')));
      },
    );
  }
}

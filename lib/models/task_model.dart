import 'dart:convert';

class TaskModel {
  String id;
  String title;
  bool isCompleted;
  DateTime time;

  TaskModel(
      {required this.id,
      required this.title,
      this.isCompleted = false,
      required this.time});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'title': title,
      'isCompleted': isCompleted ? 1 : 0,
      'time': time.millisecondsSinceEpoch.toString(),
    };
  }

  factory TaskModel.fromMap(Map<String, dynamic> map) {
    return TaskModel(
      id: map['id'] as String,
      title: map['title'] as String,
      isCompleted: (map['isCompleted'] as int == 1) ? true : false,
      time: DateTime.fromMillisecondsSinceEpoch(int.parse(map['time'] as String)),
    );
  }

  String toJson() => json.encode(toMap());

  factory TaskModel.fromJson(String source) =>
      TaskModel.fromMap(json.decode(source) as Map<String, dynamic>);

  TaskModel copyWith({
    String? id,
    String? title,
    bool? isCompleted,
    DateTime? time,
  }) {
    return TaskModel(
      id: id ?? this.id,
      title: title ?? this.title,
      isCompleted: isCompleted ?? this.isCompleted,
      time: time ?? this.time,
    );
  }

  @override
  String toString() {
    return 'TaskModel(id: $id, title: $title, isCompleted: $isCompleted, time: $time)';
  }

  @override
  bool operator ==(covariant TaskModel other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.title == title &&
        other.isCompleted == isCompleted &&
        other.time == time;
  }

  @override
  int get hashCode {
    return id.hashCode ^ title.hashCode ^ isCompleted.hashCode ^ time.hashCode;
  }
}
